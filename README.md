Bug reports were written about a delivery management application called R-Keeper. 
The application has a mobile version and PC version, which are related to each other.

Main application functionality:

  - 2 types of accounts: User and Manager (with admin rights);

  - creating orders that includes: item list, delivery address, delivery time, payment type;

  - editing already created orders;
  
  - sort orders in the list by the following characteristics: courier's name, delivery time, delivery address, payment type;

  - track who and when accepted the order for delivery;
  
  - building a route to delivery;
  
  - daily financial report on delivered orders;
  
  - closing the cashier's shift.
